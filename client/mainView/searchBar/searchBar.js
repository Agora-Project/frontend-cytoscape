Template.mainSearchBar.getParents();

let search = function(event, instance) {
    instance.posts.remove({});
    instance.actors.remove({});

    let searchTerm = instance.input.val();

    instance.searchTerm.set(searchTerm);

    if (searchTerm == "") return;

    instance.subscribe('searchTerm', searchTerm,  {
        onReady: function() {

            let actors = Actors.find({id: searchTerm});
            if (actors.count() > 0) {
                actors.forEach(function(actor) {
                    instance.actors.insert(actor);
                });
                return;
            }

            let posts = Posts.find({id: searchTerm});
            if (posts.count() > 0) {
                posts.forEach(function(post) {
                    instance.posts.insert(post);
                });
                return;
            }

            let regex = new RegExp(".*" + searchTerm + ".*", 'i');

            actors = Actors.find({preferredUsername: regex}, {limit: 50});
            if (actors.count() > 0) {
                actors.forEach(function(actor) {
                    instance.actors.insert(actor);
                });
                return
            }

            posts = Posts.find({summary: regex}, {limit: 50});
            if (posts.count() > 0) {
                posts.forEach(function(post) {
                    instance.posts.insert(post);
                });
                return;
            }
        }
    });
};

Template.mainSearchBar.onCreated(function() {
    this.searchTerm = new ReactiveVar(null);

    this.posts = new Mongo.Collection(null);
    this.actors = new Mongo.Collection(null);

    this.search = search;
});

Template.mainSearchBar.onRendered(function() {
    this.input = $('.main-search-bar-input');
});

Template.mainSearchBar.helpers({
    searchTerm: function() {
        return Template.instance().searchTerm.get();
    },
    posts: function() {
        return Template.instance().posts.find({});
    },
    actors: function() {
        return Template.instance().actors.find({});
    }
});

Template.mainSearchBar.events({
    "click .main-search-bar-button": search,
    "keydown": function(event, instance) {
        event.stopPropagation();
        if (event.key == "Enter") {
            console.log("??");
            search(event, instance);
        }
    }
});

Template.mainSearchResultsPost.onCreated(function() {
    this.subscribe('fullPost', this.data.id);
});

Template.mainSearchResultsPost.helpers({
    actor: function() {
        return Actors.findOne({id: this.attributedTo});
    },
    age: function() {
        if (this.published) {
            return new Date(this.published).toDateString();
        }
    }
});
