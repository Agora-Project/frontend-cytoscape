// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by frontend-cytoscape.js.
import { name as packageName } from "meteor/frontend-cytoscape";

// Write your tests here!
// Here is an example.
Tinytest.add('frontend-cytoscape - example', function (test) {
  test.equal(packageName, "frontend-cytoscape");
});
