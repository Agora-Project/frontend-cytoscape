/*
    Agora Forum Software
    Copyright (C) 2016 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/

Router.configure({
    layoutTemplate: 'layout'
});

Router.route('/', function() {
    return this.redirect("/recentPosts");
});

Router.route('/recentPosts', {
    onRun: function() {
        var id = this.params.query.post;

        if (id && this.ready()) {
            this.state.set("postID", id);
        }
        this.next();
    },
    action: function() {
        if (this.ready()) {
            this.render('recentPosts');
        } else this.render('errorPage', {data: {_id: id}});
    }
});

Router.route('/home', {
    action: function() {
        if (this.ready()) {
            this.render('homeTimeline');
        } else this.render('errorPage');
    }
});

Router.route('/@:handle', {
    template: 'fullProfile',
    data: function() {
        let id = Meteor.absoluteUrl() + "actor/" + this.params.handle;

        return {id: id};
    }
});

Router.route('/user', {
    action: function() {
        let user = Meteor.user();
        if (user && user.actor && this.ready()) {
            this.render('fullProfile', {data: {id: user.actor}});
        } else this.render('errorPage');
    }
});

Router.route('/@:user/:_id', {
    name: "Post",
    action: function() {
        let id = window.location.protocol + "//" + window.location.host + "/post/" + this.params._id;
        if (id && this.ready()) {
            this.render('singlePost', {data: {id: id}});
        } else this.render('errorPage');
    }
});
