/*
    Agora Forum Software
    Copyright (C) 2016 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/

/**
 * The main view consists of the following basic modules:
 *
 *    * Main Module (main.js)
 *          This file; the primary entry-point and handler for the other four modules.
 *          Callbacks are mostly set up and destroyed in this module.
 *
 *    * Layout (layout.js)
 *          Chooses positions for posts.
 *
 *    * Camera (camera.js)
 *          Stores client view state (position, zoom, screen boundaries).
 *          Also handles camera input.
 *
 *    * Partitioner (partitioner.js)
 *          An optimization module. Exposes a number of efficient spatial queries.
 *          Depends on the camera.
 *
 *    * Renderer (renderer.js)
 *          Handles the WebGL context and performs canvas rendering.
 *          Depends on the camera.
 *
 *    * Detailed Posts (detailed/detailed.js)
 *          Handles the creation of template-based HTML posts.
 *          These detailed posts appear when the camera is sufficiently zoomed.
 *          Depends on the camera and partitioner.
 *
 *    * Reply (reply/reply.js)
 *          Handles the reply/edit/report box and related code.
 */

var cytoscape = require('cytoscape');
let dagre = require('cytoscape-dagre');

cytoscape.use( dagre ); // register extension

let cy;

//If the user has panned off the graph, push them back onto it.
let limitPan = function(cy) {
    const width = cy.width();
    const height = cy.height();
    if (width <= 0 || height <= 0) return;
    const rbb = cy.elements().renderedBoundingbox();

    let pan = {x: 0, y: 0};
    let panNeeded = false;

    let gaurdSpace = 50;

    if (rbb.x1 > width - gaurdSpace) {
        pan.x = width - gaurdSpace - rbb.x1;
        panNeeded = true;
    } else if(rbb.x2 < gaurdSpace) {
        pan.x = gaurdSpace - rbb.x2;
        panNeeded = true;
    }

    if (rbb.y1 > height - gaurdSpace) {
        pan.y = height - gaurdSpace - rbb.y1;
        panNeeded = true;
    }
    else if(rbb.y2 < gaurdSpace) {
        pan.y = gaurdSpace - rbb.y2;
        panNeeded = true;
    }

    if (panNeeded) cy.panBy(pan);
}

Template.mainView.onCreated(function() {

    //Set up async notifiers.
    this.onRendered = new Notifier();
    let onSubReady = new Notifier();

    //Reactive variable for showing detailedPosts.
    this.showFullPosts = new ReactiveVar(false);

    //Variables for replying, making new posts, and editing.
    this.targetPost = new ReactiveVar();
    this.targetMode = new ReactiveVar();

    //Variables for viewing user profile previews.
    this.targetActor = new ReactiveVar();
    this.profileEditing = false;

    this.panning = false;

    let instance = this;

    this.isReplyBoxOpen = function() {
        return instance.targetMode.get() !== undefined;
    };

    if (this.data.subscription) {
        if (this.data.arg) {
            this.subscribe(this.data.subscription, this.data.arg, this.data.posts ? this.data.posts : 20, Date.now(), {onReady: onSubReady.fulfill});
        } else this.subscribe(this.data.subscription, this.data.posts ? this.data.posts : 20, Date.now(), {onReady: onSubReady.fulfill});
    }

    this.loadPost = function(postID, callback, refreshTarget) {
        if (callback === true) callback = () => {
            instance.refreshLayout(refreshTarget);
        }

        this.subscribe('abstractPostAndReplies', postID, {
            onReady: () => {
                let post = Posts.findOne({id: postID});

                if (post && !instance.getPostByID(postID)) {
                    instance.addPost(post);
                }

                if (callback) callback(post);
            },
            onError: (error) => {
                console.log(error);
                if (callback) callback(null, error);
            },
            onStop: () => {
                instance.removePost(Posts.findOne({id: postID}));
            }
        });
    }

    this.recursiveLoad = function(start, baseDegrees = 10, maxPosts = 20, relation = "Conversation") {

        let visited = [];

        let postsProcessing = 0;
        let postsLoaded = 0;

        let processPost = function(postID, degrees, relation) {

            visited.push(postID);

            if (relation === "Direct Conversation") {
                processPost(postID, degrees, "Targets");
                processPost(postID, degrees, "Replies");
                return;
            }

            if (degrees < 0 || postsLoaded >= maxPosts) {
                return;
            }

            let postAlreadyLoaded = false;
            if (!instance.getPostByID(postID)) {
                postsProcessing++;
                postsLoaded++;
            } else postAlreadyLoaded = true;

            instance.loadPost(postID, () => {
                if (relation === "Conversation" || relation === "Targets") {
                    let parentID = Posts.findOne({id: postID}).inReplyTo;

                    if (parentID && !visited.includes(parentID)) {
                        processPost(parentID, degrees - 1, relation);
                    }
                }

                if (relation === "Conversation" || relation === "Replies")
                    Posts.find({inReplyTo: postID}).forEach(function(post) {
                        if (!visited.includes(post.id)) {
                            processPost(post.id, degrees - 1, relation);
                        }
                    });

                if (!postAlreadyLoaded && --postsProcessing === 0) {
                    instance.refreshLayout(start);
                }
            });
        };

        if (Array.isArray(start)) {
            for (let id of start) {
                processPost(id, baseDegrees, relation);
            }
        } else {
            processPost(start, baseDegrees, relation);
        }
    }

    this.closePost = function(postID, callback, refreshTarget) {
        if (callback === true) callback = () => {
            instance.refreshLayout(refreshTarget);
        }

        this.removePost(Posts.findOne({id: postID}));

        if (callback) callback();
    }

    this.recursiveClose = function(start, baseDegrees = 10, maxPosts = 20, relation = "Conversation") {
        let visited = [];

        let postsClosed = 0;

        let processPost = function(postID, degrees, relation) {

            visited.push(postID);

            if (relation === "Direct Conversation") {
                processPost(postID, degrees, "Targets");
                processPost(postID, degrees, "Replies");
                return;
            }

            if (degrees < 0 || postsClosed >= maxPosts) {
                return;
            }

            let post = Posts.findOne({id: postID});

            if (!post) return;

            let close = true;

            if (relation.startsWith("Extenuating ")) {
                close = false;
                relation = relation.slice(12, relation.length);
            }

            if (relation.startsWith("Unconnected ") && cy.elements('edge[source = "' + postID + '"], edge[target = "' + postID + '"]').length > 1) {
                close = false;
            }

            if (relation.endsWith("Conversation") || relation.endsWith("Targets")) {
                let parentID = post.inReplyTo;

                if (parentID && !visited.includes(parentID)) {
                    processPost(parentID, degrees - 1, relation);
                }
            }

            if (relation.endsWith("Conversation") || relation.endsWith("Replies"))
                Posts.find({inReplyTo: postID}).forEach(function(post) {
                    if (!visited.includes(post.id)) {
                        processPost(post.id, degrees - 1, relation);
                    }
                });

            if (close) {
                instance.closePost(postID);
                postsClosed++;
            }
        };

        if (Array.isArray(start)) {
            for (let id of start) {
                processPost(id, baseDegrees, relation);
            }
        } else {
            processPost(start, baseDegrees, relation);
        }
    }

    //import a post and convert it into the right format for cytoscape, including creating entries for it's edges.
    let cytifyPost = function(post, postArray, options) {
        if (!options) options = {};

        if (options.noDuplicates && instance.getPostByID(post.id)) return;

        let node;
        let edges = [];

        node = {
            group: 'nodes',
            data: post
        };

        if (!options.skipParent && post.inReplyTo && instance.getPostByID(post.inReplyTo)) {
            edges.push({
                group: 'edges',
                data: {
                    source: post.id,
                    target: post.inReplyTo
                }
            });
        }

        if (options.checkChildren) {
            for (let prevPost of postArray) {
                if (prevPost.inReplyTo === post.id) {
                    edges.push({
                        group: 'edges',
                        data: {
                            source: prevPost.id,
                            target: post.id
                        }
                    });
                }
            }
        }

        return {node: node, edges: edges};
    }

    let initializePosts = function(posts) {
        let nodes = [];
        let edges = [];

        for (let post of posts) {
            let results = cytifyPost(post, posts);

            nodes.push(results.node);
            edges = edges.concat(results.edges);
        }

        return nodes.concat(edges);
    };

    cy = cytoscape({
        ready: function() {
        },

        style: cytoscape.stylesheet()
            .selector('node')
            .style({
                shape: 'hexagon'
            })
            .selector('edge')
            .style({
                'target-arrow-shape': 'triangle',
            }),
        maxZoom: 10.0,
        minZoom: 0.1,
        autoungrabify: true,
        autounselectify: true
    });

    let partitioner = new MainViewPartitioner(cy);

    instance.detailedPosts = new MainViewDetailedPosts(cy, instance);

    Notifier.all(onSubReady, this.onRendered).onFulfilled(function() {
        //Perform initial setup.

        let postCursor = Posts.find({});

        let container = $('#main-viewport');

        let postArray = postCursor.fetch();

        instance.addPost = function(post) {

            let results = cytifyPost(post, postArray, {checkChildren: true, noDuplicates: true});

            if (results) {
                postArray.push(post);

                cy.add(results.node);
                cy.add(results.edges);
            }
        };

        instance.getPostByID = function(id) {
            return postArray.find((otherPost) => { return otherPost.id === id; });
        }

        instance.getPostBy_ID = function(_id) {
            return postArray.find((otherPost) => { return otherPost._id === _id; })
        }

        instance.removePost = function(post) {
            let index = postArray.findIndex((otherPost) => { return otherPost.id === post.id; });

            if (index >= 0) postArray.splice(index, 1);

            cy.remove(cy.getElementById(post.id));

            partitioner.removePost(post);

            instance.detailedPosts.removePost(post);
        };

        instance.updatePost = function(_id, fields) {
            let post = postArray.find((otherPost) => { return otherPost._id === _id; });
            if (!post) return;
            for (let field in fields) {
                if (fields[field])
                    post[field] = fields[field];
                else delete post[field];
            }

            instance.detailedPosts.updatePost(_id, fields);
        };

        instance.refreshLayout = function(refreshPosition) {
            if (typeof refreshPosition === "string") {

                let oldPos = cy.getElementById(refreshPosition).renderedPosition();
                oldPos.id = refreshPosition;
                refreshPosition = oldPos;
            }

            layout.stop();

            layout = cy.elements().layout(layoutOpts);

            layout.run();

            if (Array.isArray(refreshPosition)) {
                cy.fit();
                cy.fractionalZoom(-0.02);
            } else if (refreshPosition && refreshPosition.x && refreshPosition.y) {

                let oldPos = refreshPosition;
                let newPos = cy.getElementById(refreshPosition.id).renderedPosition();

                cy.panBy({x: oldPos.x - newPos.x, y: oldPos.y - newPos.y});

                refreshPosition = false;
            }
        };

        cy.mount(container);

        cy.add(initializePosts(postArray));

        cy.getBounds = function() {
            let viewport = cy.extent();
            let bounds = {
                left: viewport.x1,
                right: viewport.x2,
                top: viewport.y2,
                bottom: viewport.y1,
                contains: function(position) {
                    return position.x > this.left && position.x < this.right && position.y > this.bottom && position.y < this.top;
                }
            };
            return bounds;
        };

        cy.getZoomFraction = function() {
            let minZoom = cy.minZoom();
            let maxZoom = cy.maxZoom();
            return (cy.zoom() - minZoom) / (maxZoom - minZoom);
        };

        cy.setZoomFraction = function(fraction) {
            let minZoom = cy.minZoom();
            let maxZoom = cy.maxZoom();
            cy.zoom({
                level: (maxZoom - minZoom) * fraction + minZoom, // the zoom level
                renderedPosition: { x: cy.width() / 2, y: cy.height() / 2}
            });
        };

        cy.fractionalZoom = function(fraction) {
            cy.setZoomFraction(cy.getZoomFraction() + fraction);
        };

        instance.getVisible = function() {
            return partitioner.getVisible(cy.getBounds());
        };

        let layoutOpts = {
            name: 'dagre',
            rankSep: -110, // Make the tree flow down, instead of up.
            nodeSep: 75,
            fit: false,
            ready: function() {
                let posts = [];

                cy.nodes().forEach(function(ele, i, eles) {
                    let post = ele.data();
                    post.position = ele.position();
                    post.replyCount = Posts.find({inReplyTo: post.id}).count();

                    posts.push(post);
                });

                partitioner.init(posts);

                postArray = posts;
                instance.detailedPosts.updatePosts(posts);
            }
        };

        let layout = cy.layout(layoutOpts);

        layout.run();

        let posts = postCursor.fetch();
        let startIDs = [];

        for (let post of posts) {
            startIDs.push(post.id);
        }

        instance.recursiveLoad(startIDs, instance.data.range ? instance.data.range : 4, 100);

        let panningStopped = _.debounce(function( e ){
            instance.panning = false;
            instance.detailedPosts.update();
        }, 50);

        cy.on("viewport", () => {
            instance.panning = true;
            limitPan(cy);

            instance.detailedPosts.updatePositionsAndUnload();

            panningStopped();
        });

        //Callback for removed posts.
        instance.postObserver = postCursor.observe({
            removed: function(post) {
                if (instance.getPostByID(post.id)) {
                    instance.removePost(post);
                    instance.refreshLayout();
                }
            }
        });

        //Callback for changed post positions.
        instance.changeObserver = postCursor.observeChanges({
            changed: function(_id, fields) {
                instance.updatePost(_id, fields);
            }
        });

    });

    this.cy = cy;
});

Template.mainView.onRendered(function() {
    this.onRendered.fulfill();
});

Template.mainView.onDestroyed(function() {
    cy.destroy();
});

Template.mainView.helpers({
    detailedPosts: function() {
        return Template.instance().detailedPosts.find();
    },
    showFullPosts: function() {
        return Template.instance().showFullPosts.get();
    },
    targetMode: function() {
        return Template.instance().targetMode.get();
    },
    targetActor: function() {
        return Template.instance().targetActor.get();
    }
});

Template.mainView.events({
    'wheel': function(event, instance) {
        event.preventDefault()
    },
    'click': function(event, instance) {
        //Close floating profile windows, unless we're dragging the view.
        if (!instance.panning && (!instance.profileEditing || confirm('You are editing your profile. Are you sure you want to close it?'))) {
            instance.targetActor.set(null);
        }

        //hide load dropdown menus, unless we're dragging the view or we just opened one.
        if (!instance.panning && !event.target.matches('.main-detailed-post-load-dropbtn')) {
            $('.main-detailed-post-load-dropdown-content').removeClass("main-detailed-post-show");
        }

        //hide close dropdown menus, unless we're dragging the view or we just opened one.
        if (!instance.panning && !event.target.matches('.main-detailed-post-close-dropbtn')) {
            $('.main-detailed-post-close-dropdown-content').removeClass("main-detailed-post-show");
        }
    }
});

Template.mainZoomControl.getParents();

Template.mainZoomControl.onRendered(function() {
    let instance = this;

    instance.slider = $('#main-zoom-control-slider');

    cy.on("zoom", function() {
        instance.slider.val(cy.getZoomFraction()*100.0);
    });
});

Template.mainZoomControl.events({
    'mousedown, touchstart, mousemove, touchmove, mouseup, touchend, wheel': function(event, instance) {
        event.stopPropagation();
    },
    'input': function() {
        let instance = Template.instance();
        cy.setZoomFraction(instance.slider.val()/100.0);
    },
    "click #main-zoom-plus-button": function(event, instance) {
        cy.fractionalZoom(0.01);
    },
    "click #main-zoom-minus-button": function(event, instance) {
        cy.fractionalZoom(-0.01);
    },
    "click #main-zoom-up-button": function(event, instance) {
        cy.panBy({x: 0, y: 50});
    },
    "click #main-zoom-down-button": function(event, instance) {
        cy.panBy({x: 0, y: -50});
    },
    "click #main-zoom-left-button": function(event, instance) {
        cy.panBy({x: 50, y: 0});
    },
    "click #main-zoom-right-button": function(event, instance) {
        cy.panBy({x: -50, y: 0});
    }
});

Template.mainReplyButton.getParents();

Template.mainReplyButton.events({
    'mousedown, touchstart, mousemove, touchmove, mouseup, touchend, wheel': function(event, instance) {
        event.stopPropagation();
    },
    "click": function(event, instance) {
        instance.parent.targetMode.set("New Post")
    }
});

//code for catching key events globally.
Template.body.events({
    "keydown": function(event, instance) {

        if (event.key.startsWith("Arrow")) {
            event.stopPropagation();
            event.preventDefault();
            switch (event.key) {
                case "ArrowLeft":
                    $('#main-zoom-left-button').click();
                    break;
                case "ArrowRight":
                    $('#main-zoom-right-button').click();
                    break;
                case "ArrowDown":
                    $('#main-zoom-down-button').click();
                    break;
                case "ArrowUp":
                    $('#main-zoom-up-button').click();
                    break;
            }

        } else if (event.key == "-") {
            event.stopPropagation();
            event.preventDefault();
            $('#main-zoom-minus-button').click();
        } else if (event.key == "+") {
            event.stopPropagation();
            event.preventDefault();
            $('#main-zoom-plus-button').click();
        }
    }
})
