import { ActivityPubActivity } from 'meteor/agoraforum:activitypub';

let DETAILED_POST_WIDTH = 100;
let DETAILED_POST_HEIGHT = 85;
let BASIC_POST_SPACING_WIDTH = 20;
let BASIC_POST_SPACING_HEIGHT = 5;
let POST_TRANSITION_ZOOM = 2;
let POST_TRANSITION_TIME = 200;

Template.mainDetailedPost.getParents();

Template.mainDetailedPost.onCreated(function() {
    let instance = this;
    let onSubReady = new Notifier();
    this.onRendered = new Notifier();
    this.seen = new ReactiveVar(true);

    this.subscribe('fullPost', this.data.id, this.data.attributedTo, {onReady: onSubReady.fulfill});

    this.subscribe('abstractPost', this.data.inReplyTo);

    Notifier.all(onSubReady, this.onRendered).onFulfilled(function() {
        //Fade out spinner and fade in actual post.
        instance.div.children('.main-detailed-post-spinner').fadeOut(100);

        instance.div.children('.main-detailed-post-flex')
            .css('display', 'flex')
            .hide()
            .fadeIn(POST_TRANSITION_TIME);


        if (!postIsSeen(instance.data)) {
            instance.seen.set(false);
            instance.div.addClass('unseen');
            Meteor.call('addSeenPost', instance.data._id);
        }

        instance.parent.detailedPosts.sizeDetailedPost(instance.data);
        instance.parent.detailedPosts.centerPost(instance.data);
    });
});

Template.mainDetailedPost.onRendered(function() {
    this.div = $('#main-detailed-post-' + this.data._id);
    this.div.css('display', 'flex').hide().fadeIn(POST_TRANSITION_TIME);
    setTimeout(this.onRendered.fulfill, 250);
});

Template.mainDetailedPost.helpers({
    actor: function() {
        return Actors.findOne({id: this.attributedTo});
    },
    initials: function() {
        return Actors.findOne({id: this.attributedTo}).name[0];
    },
    age: function() {
        if (this.published) {
            return new Date(this.published).toDateString();
        }
    },
    currentUser: function() {
        return (Meteor.user());
    },
    verifiedUser:function() {
        let user = Meteor.user();
        return (user.emails && user.emails.length > 0 && user.emails[0].verified);
    },
    editAccess: function() {
        return this.attributedTo === Meteor.user().actor || Roles.userIsInRole(Meteor.userId(), ['moderator']);
    },
    isModerator: function() {
        return Roles.userIsInRole(Meteor.userId(), ['moderator']);
    },
    hasReplyButtons: function() {
        let instance = Template.instance();
        Meteor.setTimeout(function () {
            instance.parent.detailedPosts.sizeDetailedPost(instance.data);
            instance.parent.detailedPosts.centerPost(instance.data);
        }, 10);

        return !instance.parent.isReplyBoxOpen();
    },
    hasLoadButton: function() {
        let instance = Template.instance();
        let ret = false;
        Posts.find({$or: [{id: this.inReplyTo}, {inReplyTo: this.id}]}).forEach(function(post) {
            if (!ret && !instance.parent.getPostByID(post.id)) ret = true;
        });

        Meteor.setTimeout(function () {
            instance.parent.detailedPosts.sizeDetailedPost(instance.data);
            instance.parent.detailedPosts.centerPost(instance.data);
        }, 10);
        return ret;
    },
    seen: function() {
        let seen = Template.instance().seen.get();
        return Template.instance().seen.get();
    },
    likes: function() {
        let likeList = LikeLists.findOne({id: this.likes}, {fields: {totalItems: true}});
        if (likeList)
            return likeList.totalItems;
    },
    shares: function() {
        let shareList = ShareLists.findOne({id: this.shares}, {fields: {totalItems: true}});
        if (shareList)
            return shareList.totalItems;
    },
    likesOrShares: function() {
        let likeList = LikeLists.findOne({id: this.likes}, {fields: {totalItems: true}});
        if (likeList && likeList.totalItems > 0) return true;

        let shareList = ShareLists.findOne({id: this.shares}, {fields: {totalItems: true}});
        if (shareList && shareList.totalItems > 0) return true;

        return false;
    },
    likesAndShares: function() {
        let likeList = LikeLists.findOne({id: this.likes}, {fields: {totalItems: true}});
        if (!likeList || likeList.totalItems == 0) return false;

        let shareList = ShareLists.findOne({id: this.shares}, {fields: {totalItems: true}});
        if (!shareList || shareList.totalItems == 0) return false;

        return true;
    },
    liked: function() {

        let actorID = Meteor.user().actor;
        let actor = Actors.findOne({id: actorID});
        if (!actor) return false;

        return Activities.findOne({actor: actorID, type: "Like", object: this.id});
    },
    shared: function() {

        let actorID = Meteor.user().actor;
        let actor = Actors.findOne({id: actorID});
        if (!actor) return false;

        return Activities.findOne({actor: actorID, type: "Announce", object: this.id});
    }
});

Template.mainDetailedPost.events({
    'click .main-detailed-post-avatar, click .main-detailed-post-username': function(event, instance) {
        event.preventDefault();
        event.stopPropagation();

        instance.parent.targetActor.set(this.attributedTo);
    },
    'mousedown, touchstart, mousemove, touchmove, mouseup, touchend, wheel': function(event, instance) {
        if (instance.parent.panning) {
            //Prevents interaction while dragging.
            event.preventDefault();
        }
        else {
            //Prevent events from passing through posts into the WebGL canvas.
            event.stopPropagation();
        }
    },
    'click .main-detailed-post-reply-button': function(event, instance) {
        //Our parent is a mainDetailedPost, and its parent is the mainView.
        instance.parent.targetPost.set(instance.data);
        instance.parent.targetMode.set("Reply");
    },
    'click .main-detailed-post-edit-button': function(event, instance) {
        //Our parent is a mainDetailedPost, and its parent is the mainView.
        instance.parent.targetPost.set(instance.data);
        instance.parent.targetMode.set("Edit");
    },
    'click .main-detailed-post-delete-button': function(event, instance) {
        //Our parent is a mainDetailedPost, and its parent is the mainView.
        if (confirm("Are you sure you want to delete this post?")) {

            let actorID = Meteor.user().actor;

            let activity = new ActivityPubActivity("Delete", actorID, instance.data.id);
            activity.copyAddressingProperties(instance.data);

            if (instance.data.attributedTo != actorID) activity.to.push(instance.data.attributedTo);

            Meteor.call('postActivity', activity);
        }
    },
    'click .main-detailed-post-load-dropbtn': function(event, instance) {
        let dropdownMenu = instance.$('#main-basic-post-load-dropdown-' + this._id);

        dropdownMenu.toggleClass("main-detailed-post-show");
    },
    'click .main-detailed-post-load-adjacent-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, 1);
    },
    'click .main-detailed-post-load-target-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, 1, 20, "Targets");
    },
    'click .main-detailed-post-load-replies-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, 1, 20, "Replies");
    },
    'click .main-detailed-post-load-targets-recursively-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, Infinity, 20, "Targets");
    },
    'click .main-detailed-post-load-replies-recursively-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, Infinity, 20, "Replies");
    },
    'click .main-detailed-post-load-direct-conversation-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, Infinity, 20, "Direct Conversation");
    },
    'click .main-detailed-post-load-conversation-button': function(event, instance) {
        instance.parent.recursiveLoad(instance.data.id, Infinity);
    },
    'click .main-detailed-post-close-dropbtn': function(event, instance) {
        let dropdownMenu = instance.$('#main-basic-post-close-dropdown-' + this._id);

        dropdownMenu.toggleClass("main-detailed-post-show");
    },
    'click .main-detailed-post-close-button': function(event, instance) {
        instance.parent.closePost(this.id);
    },
    'click .main-detailed-post-close-adjacent-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, 1, Infinity, "Extenuating Conversation");
    },
    'click .main-detailed-post-close-unconnected-adjacent-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, 1, Infinity, "Extenuating Unconnected Conversation");
    },
    'click .main-detailed-post-close-target-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, 1, Infinity, "Extenuating Targets");
    },
    'click .main-detailed-post-close-replies-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, 1, Infinity, "Extenuating Replies");
    },
    'click .main-detailed-post-close-targets-recursively-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, Infinity, Infinity, "Extenuating Targets");
    },
    'click .main-detailed-post-close-replies-recursively-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, Infinity, Infinity, "Extenuating Replies");
    },
    'click .main-detailed-post-close-direct-conversation-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, Infinity, Infinity, "Direct Conversation");
    },
    'click .main-detailed-post-close-conversation-button': function(event, instance) {
        instance.parent.recursiveClose(instance.data.id, Infinity, Infinity);
    },
    'click .main-detailed-post-like-button': function(event, instance) {
        let actorID = Meteor.user().actor;
        let actor  = Actors.findOne({id: actorID});
        Meteor.call("postActivity", {
            actor: actorID,
            type: "Like",
            object: instance.data.id,
            to: [instance.data.attributedTo],
            cc: [actor.followers],
            bto: [],
            bcc: [],
            audience: []
        }, function(error, result) {

        });
    },
    'click .main-detailed-post-unlike-button': function(event, instance) {
        let actorID = Meteor.user().actor;
        let actor  = Actors.findOne({id: actorID});
        let activity = Activities.findOne({actor: actorID, type: "Like", object: this.id});

        Meteor.call("postActivity", {
            actor: actorID,
            type: "Undo",
            object: activity.id,
            to: [instance.data.attributedTo],
            cc: [actor.followers],
            bto: [],
            bcc: [],
            audience: []
        }, function(error, result) {
        });
    },
    'click .main-detailed-post-share-button': function(event, instance) {
        let actorID = Meteor.user().actor;
        let actor  = Actors.findOne({id: actorID});
        Meteor.call("postActivity", {
            actor: actorID,
            type: "Announce",
            object: instance.data.id,
            to: [instance.data.attributedTo],
            cc: [actor.followers],
            bto: [],
            bcc: [],
            audience: []
        }, function(error, result) {

        });
    },
    'click .main-detailed-post-unshare-button': function(event, instance) {
        let actorID = Meteor.user().actor;
        let actor  = Actors.findOne({id: actorID});
        let activity = Activities.findOne({actor: actorID, type: "Announce", object: this.id});

        Meteor.call("postActivity", {
            actor: actorID,
            type: "Undo",
            object: activity.id,
            to: [instance.data.attributedTo],
            cc: [actor.followers],
            bto: [],
            bcc: [],
            audience: []
        }, function(error, result) {
        });
    },
});


Template.mainBasicPost.getParents();

Template.mainBasicPost.onCreated(function() {
    let instance = this;
    let onSubReady = new Notifier();
    this.onRendered = new Notifier();
    this.seen = new ReactiveVar(true);

    this.subscribe('fullPost', this.data.id, this.data.attributedTo, {onReady: onSubReady.fulfill});

    Notifier.all(onSubReady, this.onRendered).onFulfilled(function() {
        //Fade out spinner and fade in actual post.
        instance.div.children('.main-basic-post-spinner').fadeOut(100);
        instance.div.css('overflow', 'visible');
        instance.div.children('.main-basic-post-flex')
            .css('display', 'flex')
            .hide()
            .fadeIn(200);

        if (!postIsSeen(instance.data)) {
            instance.seen.set(false);
            instance.div.addClass('unseen');
        }

        instance.parent.detailedPosts.centerPost(instance.data);
        instance.parent.detailedPosts.checkCollisions(instance.data);
    });
});


Template.mainBasicPost.onRendered(function() {
    this.div = $('#main-basic-post-' + this.data._id);
    this.div.css('display', 'flex').hide().fadeIn(200);
    setTimeout(this.onRendered.fulfill, 250);
});

Template.mainBasicPost.helpers({
    actor: function() {
        return Actors.findOne({id: this.attributedTo});
    },
    initials: function() {
        return Actors.findOne({id: this.attributedTo}).name[0];
    },
    preview: function() {
        if (this.summary) return this.summary.slice(0, 20);
        else if (this.content) return this.content.slice(0, 20);
    },
    seen: function() {
        let seen = Template.instance().seen.get();
        return Template.instance().seen.get();
    }
});

MainViewDetailedPosts = function(cy, mainView) {
    let instance = this;

    //Collection of currently visible detailed posts.
    let visiblePosts = new Mongo.Collection(null);

    this.addVisiblePost = function(post) {
        visiblePosts.insert(post);
    };

    this.removePost = function(post) {

        let div;

        if (mainView.showFullPosts.get()) {
            div = $('#main-detailed-post-' + post._id);
        } else {
            div = $('#main-basic-post-' + post._id);
        }

        div.fadeOut(POST_TRANSITION_TIME, function() {
            visiblePosts.remove({_id: post._id});
        });
    };

    this.updatePost = function(_id, fields) {

        let post;

        if (visiblePosts.findOne({_id: _id})) {
            visiblePosts.update({_id: _id}, {$set: fields});
            post = visiblePosts.findOne({_id: _id});
        } else {
            post = mainView.getPostBy_ID(_id);

            if (instance.postVisible(post)) {
                instance.addVisiblePost(post);
            }
        }

        if (!mainView.showFullPosts.get()) { //If we're zoomed out far enough to show labels,
            visiblePosts.update({id: post.id}, {$set: {hidden : false}});
            $('#main-basic-post-' + post._id).css('visibility', 'visible'); //Show them,

            instance.centerPost(post); //Center them,

            instance.checkCollisions(post); //Then check if they need to be hidden.

        } else { //Or, if we're zoomed in far enough to show detailed posts,
            instance.sizeDetailedPost(post); //make sure they're sized correctly,

            instance.centerPost(post); //and center them.
        }
    };

    this.updatePosts = function(posts) {
        for (let post of posts) {
            this.updatePost(post._id, post);
        }
    }

    this.getDiv = function(post) {
        if (mainView.showFullPosts.get())
            return $('#main-detailed-post-' + post._id);
        else return $('#main-basic-post-' + post._id);
    }

    this.centerPost = function(post) {
        let div = this.getDiv(post);

        let pos = cy.getElementById(post.id).renderedPosition();

        if (!pos) return;

        let offset = div.offset();

        if (offset) {

            let diffLeft = offset.left - (pos.x - div.outerWidth()/2);
            let diffTop = offset.top - (pos.y - div.outerHeight()/2);

            div.css('left', pos.x - div.outerWidth()/2);
            div.css('top', pos.y - div.outerHeight()/2);
        } else {
            div.css('left', pos.x - div.outerWidth()/2);
            div.css('top', pos.y - div.outerHeight()/2);
        }
    };

    this.checkCollisions = function(post) {
        let div = $('#main-basic-post-' + post._id);

        if (!post.hidden) {

            let pos = cy.getElementById(post.id).renderedPosition();

            if (!pos) return;

            //This is for deciding how many adjacent posts to look for and check collisions against. Magic numbers are for max width and height of a preview.
            let width = 1 + Math.floor(175/cy.zoom()), height = 1 + Math.floor(25/cy.zoom());

            let collisionPosts = visiblePosts.find({
                "position.x": {$gte: post.position.x - width, $lte: post.position.x + width},
                "position.y": {$gte: post.position.y - height, $lte: post.position.y + height}
                });

            for (let post2 of collisionPosts) {
                if (!post2 || post2.hidden || post.id === post2.id) continue;

                let div2 = $('#main-basic-post-' + post2._id);

                let pos2 = cy.getElementById(post2.id).renderedPosition();

                if (!pos2) return;

                let outerWidth1 = div.outerWidth()/2;
                let outerWidth2 = div2.outerWidth()/2;

                let outerHeight1 = div.outerHeight()/2;
                let outerHeight2 = div2.outerHeight()/2;

                //Checks for collisions, with a buffer space.
                if ((pos.x + outerWidth1 + BASIC_POST_SPACING_WIDTH) > pos2.x - outerWidth2 &&
                    pos.x - outerWidth1 < (pos2.x + outerWidth2 + BASIC_POST_SPACING_WIDTH) &&
                    pos.y + outerHeight1 + BASIC_POST_SPACING_HEIGHT > pos2.y - outerHeight2 &&
                    pos.y - outerHeight1 < pos2.y + outerHeight2 + BASIC_POST_SPACING_HEIGHT) {

                    if (post2.replyCount <= post.replyCount) {
                        visiblePosts.update({id: post2.id}, {$set: {hidden: true}});
                        div2.css('visibility', 'hidden');
                    } else {
                        visiblePosts.update({id: post.id}, {$set: {hidden: true}});
                        div.css('visibility', 'hidden');
                    }
                }
            }
        } else {
            div.css('visibility', 'hidden');
        }
    };

    this.sizeDetailedPost = function(post) {
        let div = $('#main-detailed-post-' + post._id);

        let pos = cy.getElementById(post.id).renderedPosition();
        if (!pos) return;

        let postWidth = DETAILED_POST_WIDTH*cy.zoom() - 10;


        div.css('max-width', postWidth);

        let postMaxHeight = DETAILED_POST_HEIGHT*cy.zoom() - 10;

        let divMaxHeight = div.css('max-height');
        if (divMaxHeight && divMaxHeight !== "none") {

            divMaxHeight = parseFloat(divMaxHeight);

            div.css('max-height', postMaxHeight);
        } else div.css('max-height', DETAILED_POST_HEIGHT*cy.zoom());
    };

    this.postVisible = function(post) { //Should a post be visible?
        return ((mainView.showFullPosts.get() || (1 + post.replyCount) * cy.zoom() > 1) && cy.getBounds().contains(post.position)); //Depends on how many replies it has, versus how far we're zoomed in.
    };

    this.updatePositions = function() {
        visiblePosts.find({}).forEach(function(post, i) {
            instance.centerPost(post);
        });
    }

    this.updatePositionsAndUnload = function() {
        if (mainView.showFullPosts.get()) { //if we're showing full posts
            if (cy.zoom() < POST_TRANSITION_ZOOM) { //if we're zoomed out far enough
                mainView.showFullPosts.set(false); //show the basic versions.
            }
        }

        visiblePosts.find({}).forEach(function(post) {
            if (!instance.postVisible(post)) { //If a posts center is off screen, or it has too few replies relative to how far we're zoomed in,
                instance.removePost(post); //stop showing it.
            }
        });

        //If we're zoomed out far enough to show labels
        if (!mainView.showFullPosts.get()) {
            //sort posts by priority.
            let visiblePostsByPriority = visiblePosts.find({hidden: false}, {sort: {replyCount: -1}});

            //go through the sorted posts and hide the ones with less priority whenever they would overlap.
            visiblePostsByPriority.forEach(function(post, i) {

                instance.checkCollisions(post);

                instance.centerPost(post);

            });

        } else {
            visiblePosts.find({}).forEach(function(post, i) {
                instance.sizeDetailedPost(post);

                instance.centerPost(post);
            });
        }
    }

    this.update = function() {

        if (mainView.showFullPosts.get()) { //if we're showing full posts
            if (cy.zoom() < POST_TRANSITION_ZOOM) { //if we're zoomed out far enough
                mainView.showFullPosts.set(false); //show the basic versions.
            }
        } else { //if we're not showing full posts
            if (cy.zoom() >= POST_TRANSITION_ZOOM) { //if we're zoomed in far enough
                let div = $('.main-basic-post'); //find the basic posts

                if (div.length > 0) div.fadeOut(POST_TRANSITION_TIME, function() { //fade them out,
                        mainView.showFullPosts.set(true); //and show the full versions.
                    });
                else {
                    mainView.showFullPosts.set(false); //and show the basic versions.
                }
            }
        }

        visiblePosts.find({}).forEach(function(post) {
            if (!instance.postVisible(post)) { //If a posts center is off screen, or it has too few replies relative to how far we're zoomed in,
                instance.removePost(post); //stop showing it.
            }
        });

        //Add posts which are newly visible.
        let visible = mainView.getVisible();
        for (let post of visible) {
            if (!visiblePosts.findOne({_id: post._id}) && instance.postVisible(post)) {
                instance.addVisiblePost(post);
            }
        }

        //If we're zoomed out far enough to show labels
        if (!mainView.showFullPosts.get()) {
            visiblePosts.update({}, {$set: {hidden: false}}, {multi: true});
            $('.main-basic-post').css('visibility', 'visible');

            //sort posts by priority.
            let visiblePostsByPriority = visiblePosts.find({}, {sort: {replyCount: -1}});

            //go through the sorted posts and hide the ones with less priority whenever they would overlap.
            visiblePostsByPriority.forEach(function(post, i) {

                instance.checkCollisions(post);

                instance.centerPost(post);

            });

        } else {
            visiblePosts.find({}).forEach(function(post, i) {
                instance.sizeDetailedPost(post);

                instance.centerPost(post);
            });
        }
    };

    this.find = function() {
        return visiblePosts.find({});
    };
};
