/*
    Agora Forum Software
    Copyright (C) 2016 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/

Package.describe({
    name: 'agoraforum:frontend-cytoscape',
    version: '0.2.3',

    summary: 'Cytoscape frontend for the Agora forum software.',

    git: 'https://gitlab.com/Agora-Project/frontend-cytoscape',

    documentation: null,
    //documentation: 'README.md'
    license: "LICENSE"
});

Npm.depends({
    'cytoscape': '3.4.2',
    'cytoscape-dagre': '2.2.2'
});

Package.onUse(function(api) {
    api.versionsFrom('1.8.0.2');

    api.use([
        'iron:router@1.1.2',
        'accounts-password',
        'alanning:roles@1.2.16',
        'blaze-html-templates@1.1.2',
        'verron:autosize@3.0.8',
        'ecmascript',
        'accounts-ui',
        'underscore',
        'gwendall:body-events'
    ]);

    api.addFiles([
        'client/lib/templateParents.js',
        'client/lib/notifier.js',
        'client/lib/partitioner.js',
        'client/lib/seenPosts.js',
        'client/main.html',
        'client/main.css',
        'client/mainView/mainView.html',
        'client/mainView/mainView.css',
        'client/mainView/mainView.js',
        'client/mainView/detailed/detailed.html',
        'client/mainView/detailed/detailed.css',
        'client/mainView/detailed/detailed.js',
        'client/mainView/reply/reply.html',
        'client/mainView/reply/reply.css',
        'client/mainView/reply/reply.js',
        'client/mainView/searchBar/searchBar.html',
        'client/mainView/searchBar/searchBar.css',
        'client/mainView/searchBar/searchBar.js',
        'client/userProfile/userProfile.html',
        'client/userProfile/userProfile.css',
        'client/userProfile/userProfile.js',
        'client/init.js'
    ], 'client');

    api.addFiles([
        'client/routes.js'
    ]);

});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('agoraforum:frontend-cytoscape');
    api.mainModule('frontend-cytoscape-tests.js');
});
